import React, {useEffect} from 'react';

import ProfileFriendsList from '../profile-friends-list/profile-friends-list.component.jsx';
import ProfileInviationsList from '../invitations/invitation-list.component.jsx';
import ProfileUsersList from '../profile-users-list/profile-users-list.component';
import {useCurrentUser} from "../../../contexts/CurrentUserContext";
import useLogout from "../../../hooks/useLogout";
import UpdateProfile from './update-profile.component';

const ProfileInfo = (props) => {

    var styles = null;

    if (props.currentUser) {
        if (props.currentUser.role === "ROLE_ADMIN") {
            styles = {
                display: "inline",
                float: "right",
                marginBottom: "20px"
            }
        } else {
            styles = {
                display: "none",
            }
        }
    }

    console.log(props.currentUser);
    return (
        <>
            <div className="col-lg-9 col-md-8 col-sm-8">
                <div className="main-box clearfix">
                    <div className="profile-header">
                        <h3>
                            <span>User info</span>
                        </h3>
                        <a href="#" data-toggle="modal" data-target="#basicModal"
                           className="btn btn-primary edit-profile">
                            <i className="fa fa-pencil-square fa-lg"/> Edit profile
                        </a>
                        <div style={styles}>
                            <a className="btn btn-info" href="/logs">Logs</a>
                            <a className="btn btn-warning" href="/analytics">Analytics</a>
                            <a className="btn btn-success" href="/reports">Reports</a>
                        </div>
                    </div>
                    <UpdateProfile/>
                    <div className="row profile-user-info">
                        <div className="col-sm-8">
                            <div className="profile-user-details clearfix">
                                <div className="profile-user-details-label">First Name</div>
                                <div className="profile-user-details-value">{props.currentUser?.firstName}</div>
                            </div>
                            <div className="profile-user-details clearfix">
                                <div className="profile-user-details-label">Last Name</div>
                                <div className="profile-user-details-value">{props.currentUser?.lastName}</div>
                            </div>
                            <div className="profile-user-details clearfix">
                                <div className="profile-user-details-label">Email</div>
                                <div className="profile-user-details-value">
                                    {props.currentUser?.email}
                                </div>
                            </div>
                            <div className="profile-user-details clearfix">
                                <div className="profile-user-details-label">Technologies</div>
                                <div className="profile-user-details-value">
                                    {props.currentUser?.technologies.map(function (techno) {
                                        return techno.title;
                                    }).join(', ')}
                                </div>
                            </div>
                        </div>
                        <div className="tabs-wrapper profile-tabs">
                            <ul className="nav nav-tabs">
                                <li className="active">
                                    <a href="#tab-friends" data-toggle="tab">
                                        Friends
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab-invitations" data-toggle="tab">
                                        Invitations
                                    </a>

                                </li>
                                <li>
                                    <a href="#tab-users" data-toggle="tab">
                                        Users
                                    </a>
                                </li>
                            </ul>
                            <div className="tab-content">
                                <ProfileFriendsList/>
                                <ProfileInviationsList/>
                                <ProfileUsersList/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProfileInfo;