import React, {useEffect, useState} from 'react';
import { SignupMultiSelect } from '../sign-up-multiselect';
import { httpUpdateProfileInfos } from '../../../hooks/requests';

const UpdateProfileInfo = () => {
    const [selectedTechnologies, setSelectedTechnologies] = useState([]);
    const submitModification = async (e) => {
        e.preventDefault();
        const data = new FormData(e.target);
        let profileInfo = {};

        const firstName = data.get("firstName");
        if (firstName) {
          profileInfo.firstName = firstName;
        }
        const lastName = data.get("lastName");
        if (lastName) {
          profileInfo.lastName = lastName;
        }
        const password = data.get("password");
        if (password) {
          profileInfo.password = password;
        }
        const user_technologies = selectedTechnologies;

        const response = await httpUpdateProfileInfos(profileInfo);
        
        if (response.ok) {
          window.location.reload();
        } else {
            console.log(JSON.stringify(response));
        }
    };

    const handleSelectChange = (selectedOptions) => {
        let selectedOptionsArray = selectedOptions.map((val, _) => {
            return {technology_id:val.value};
        });
        setSelectedTechnologies(selectedOptionsArray);
    };
    return (
<div
  className="modal fade"
  id="basicModal"
  tabIndex={-1}
  role="dialog"
  aria-labelledby="basicModal"
  aria-hidden="true"
>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-header">
        <button
          type="button"
          className="close"
          data-dismiss="modal"
          aria-hidden="true"
        >
          ×
        </button>
        <h4 className="modal-title" id="myModalLabel">
          Update Profile
        </h4>
      </div>
      <div className="modal-body">
      <form className="form-signin" onSubmit={submitModification}>
            <input
              type="firstName"
              id="inputFirstName"
              name="firstName"
              className="form-control"
              placeholder="First name"
              required=""
              autoFocus=""
            />
            <input
              type="lastName"
              id="inputFirstName"
              name="lastName"
              className="form-control"
              placeholder="Last name"
              required=""
              autoFocus=""
            />
            <SignupMultiSelect handleChange={handleSelectChange}/>
            <input
              type="password"
              id="inputPassword"
              name="password"
              className="form-control"
              placeholder="Password"
              required=""
            />
            <hr />
            <button className="btn btn-success btn-block" type="submit" id="btn-signup">
              Update Profile
            </button>
          </form>
      </div>
    </div>
  </div>
</div>    );
}

export default UpdateProfileInfo;