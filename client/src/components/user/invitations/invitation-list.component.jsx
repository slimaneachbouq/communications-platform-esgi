import React, {useState, useEffect} from "react";
import {httpGetInvitations} from "../../../hooks/requests";
import createFriendRequest, {updateFriendRequest} from "../../../hooks/useFriendRequest";

const handleUpdateInvitation = (e, data) => {
    var sender = data.id;
    var choice = e.target.name === "confirm";
    document.getElementById("confirm_" + sender).disabled = true;
    document.getElementById("refuse_" + sender).disabled = true;

    return updateFriendRequest(sender, choice);
}

const ProfileInviationsList = () => {
    const [invitations, setInvitations] = useState([]);

    useEffect(() => {
        const getUserInvitations = async () => {
            const response = await httpGetInvitations();
            
            if (response.status === 200) {
                const body = await response.json();
                setInvitations(body);
            } else {
                console.log(body);
            }
        }

        getUserInvitations();
    }, []);


    return (
        <div className="tab-pane fade in" id="tab-invitations">
            <ul className="widget-users row">
                {invitations.map((invitation) => {
                        return (
                            <li key={invitation.userIdSender.id} className="col-md-6">
                                <div className="img">
                                    <img
                                        src="https://bootdey.com/img/Content/avatar/avatar1.png"
                                        className="img-responsive"
                                        alt=""
                                    />
                                </div>
                                <div className="details">
                                    <div className="name">
                                        <a href="#">{invitation.userIdSender.firstName} {invitation.userIdSender.lastName} </a>
                                    </div>
                                </div>
                                <div>
                                    <input type="button" className="btn btn-primary btn-sm" value="Confirm" id={"confirm_" + invitation.userIdSender.id}
                                           name="confirm" onClick={((e) => handleUpdateInvitation(e, invitation.userIdSender))}/>
                                    <input type="button" className="btn btn-danger btn-sm" value="Refuse" id={"refuse_" + invitation.userIdSender.id}
                                           name="refuse" onClick={((e) => handleUpdateInvitation(e, invitation.userIdSender))}/>
                                </div>
                            </li>
                        )
                    }
                )}
            </ul>
            <br/>
        </div>
    )
}

export default ProfileInviationsList;