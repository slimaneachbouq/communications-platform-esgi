import React from 'react';
import moment from 'moment';
import { useNavigate } from 'react-router-dom';

const ProfileBox = (props) => {
  const navigate = useNavigate();
  const navigateToMessages = () => {
    navigate('/messages');
  };
    return (
        <>
      <div className="col-lg-3 col-md-4 col-sm-4">
        <div className="main-box clearfix">
          <h2>{props.currentUser?.firstName} {props.currentUser?.lastName}</h2>
          <div className="profile-status">
            <i className="fa fa-check-circle" /> Online
          </div>
          <img
            src="https://bootdey.com/img/Content/avatar/avatar1.png"
            alt=""
            className="profile-img img-responsive center-block"
          />
          <div className="profile-since">Member since: {moment(props.currentUser?.createdAt).format('DD/MM/YYYY')}</div>
          <div className="profile-message-btn center-block text-center">
                    <a onClick={navigateToMessages} className="btn btn-success">
                        <i className="fa fa-envelope"></i> Messages
                    </a>
          </div>
        </div>
      </div>
    </>
    )
}

export default ProfileBox;