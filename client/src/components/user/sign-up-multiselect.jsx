import { useCallback, useEffect, useState } from "react";
import Select from "react-select";

import useTechnologies from "../../hooks/useTechnologies";

export const SignupMultiSelect = (props) => {
    const technologies =  useTechnologies();

    return (
        <>
            <Select
                isMulti
                options={technologies}
                onChange={props.handleChange}
            />
        </>
    );
};