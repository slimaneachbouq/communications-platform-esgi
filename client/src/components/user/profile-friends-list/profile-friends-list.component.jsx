import React, {useState, useEffect} from "react";
import {getFriends} from "../../../hooks/requests";
import {removeFriendRequest, updateFriendRequest} from "../../../hooks/useFriendRequest";
import {createReport} from "../../../hooks/useReports";
import {useCurrentUser} from "../../../contexts/CurrentUserContext";

const handleRemoveInvitation = (e, data) => {
    var friend = data.id;
    document.getElementById("remove_" + friend).disabled = true;

    return removeFriendRequest(friend);
}

var loggedUser = null;
const handleNewReport = (e, data) => {
    var createdBy = loggedUser.email;
    var reportedUser = data.email;
    var reason = prompt("Please enter a reason for your report on user " + data.firstName + " " + data.lastName);
    if (!reason) {
        return navigate("/profile");
    }

    return createReport(createdBy, reportedUser, reason);
}

const ProfileFriendsList = () => {
    const {currentUser, fetchCurrentUser} = useCurrentUser();
    useEffect(() => {
        fetchCurrentUser();
    }, []);

    if (currentUser) {
        loggedUser = currentUser.user;
    }
    const [friends, setFriends] = useState([]);

    useEffect(() => {
        const getUserFriends = async () => {
            const response = await getFriends();
            if (response.status === 200) {
                const body = await response.json();
                setFriends(body);
            } else {
                console.error(response);
            }

        }
        getUserFriends();
    }, []);


    return (
        <div className="tab-pane fade in active" id="tab-friends">
            <ul className="widget-users row">
                {friends.map((user, index) => {
                        return (
                            <li key={user.id} className="col-md-6">
                                <div className="img">
                                    <img
                                        src="https://bootdey.com/img/Content/avatar/avatar1.png"
                                        className="img-responsive"
                                        alt=""
                                    />
                                </div>
                                <div className="details">
                                    <div className="name">
                                        <a href="#">{user.firstName} {user.lastName} </a>
                                    </div>
                                    <input type="button" className="btn btn-danger btn-sm" value="Delete"
                                           id={"remove_" + user.id}
                                           onClick={((e) => handleRemoveInvitation(e, user))}/>
                                    <input type="button" className="btn btn-danger btn-sm" value="Report"
                                           onClick={((e) => handleNewReport(e, user))}/>
                                </div>
                            </li>
                        )
                    }
                )}
            </ul>
            <br/>

        </div>
    )
}

export default ProfileFriendsList;