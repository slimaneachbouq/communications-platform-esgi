import React, {useCallback, useEffect, useState} from "react";
import {httpGetUsersNotRelatedToAuthUser} from '../../../hooks/requests';
import {useNavigate} from "react-router-dom";
import useUserInfo from "../../../hooks/useUserInfo";
import {createReport} from "../../../hooks/useReports";
import createFriendRequest from "../../../hooks/useFriendRequest";

const handleNewInvitation = (e, data) => {
    var receiver = data.id;
    document.getElementById(e.target.id).disabled = true;
    return createFriendRequest(receiver);
}

var navigate = null;
var loggedUser = null;

const handleNewReport = (e, data) => {
    var createdBy = loggedUser.email;
    var reportedUser = data.email;
    var reason = prompt("Please enter a reason for your report on user " + data.firstName + " " + data.lastName);
    if (!reason) {
        return navigate("/profile");
    }

    return createReport(createdBy, reportedUser, reason);
}

const ProfileUsersList = () => {
    navigate = useNavigate();
    loggedUser = useUserInfo();
    if (loggedUser.length === undefined) {
        loggedUser = loggedUser.user;
    }

    const [users, setUsers] = useState([]);
    const getUsers = useCallback(async () => {
        const response = await httpGetUsersNotRelatedToAuthUser();
        const body = await response.json();
        if (response.status === 200) {
            setUsers(body);
        }
    }, []);

    useEffect(() => {
        getUsers();
    }, [getUsers]);
    return (
        <div className="tab-pane fade in" id="tab-users">
            <ul className="widget-users row">
                {users.map((user) => (
                    <li key={user.id} className="col-md-6">
                        <div className="img">
                            <img
                                src="https://bootdey.com/img/Content/avatar/avatar1.png"
                                className="img-responsive"
                                alt=""
                            />
                        </div>
                        <div className="details">
                            <div className="name">
                                <a href="#">{user.firstName} {user.lastName}</a>
                            </div>
                            <div className="type">
                                <input id={"sendBtn_" + user.id} type="button" className="btn btn-success btn-sm"
                                       value="Send friend request"
                                       onClick={((e) => handleNewInvitation(e, user))}/>
                                <input type="button" className="btn btn-danger btn-sm" value="Report"
                                       onClick={((e) => handleNewReport(e, user))}/>
                            </div>
                        </div>
                    </li>
                ))}
            </ul>
            <br/>
        </div>
    )
}

export default ProfileUsersList;