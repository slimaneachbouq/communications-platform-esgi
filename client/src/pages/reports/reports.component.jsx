import React from 'react';

import {useNavigate} from 'react-router-dom';

import './reports.styles.css';
import useReport from "../../hooks/useReports";

const Reports = () => {
    const navigate = useNavigate();

    var data = useReport();
    if (data.length === undefined) {
        data = data.reports;
    }

    /*const navigateToHomepage = () => {
        navigate('/');
    };*/
    return (
        <div id="logs-section" className="text-center">
            <h1>Reports table</h1>
            <div style={{float: "left"}}>
                <a href="/profile" className="btn btn-primary">Profile</a>
                <a href="/logs" className="btn btn-info">Logs</a>
                <a href="/analytics" className="btn btn-warning">Analytics</a>
            </div>
            <table className="table table-responsive">
                <thead>
                <tr>
                    <th style={{textAlign: 'center'}}>Created by user</th>
                    <th style={{textAlign: 'center'}}>User reported</th>
                    <th style={{textAlign: 'center'}}>Reason</th>
                    <th style={{textAlign: 'center'}}>Date</th>
                </tr>
                </thead>
                <tbody>
                {data.map(report => (
                    <tr key={Math.random().toString(36).substr(2, 9)}>
                        <td>{report.createdBy}</td>
                        <td>{report.userReported}</td>
                        <td>{report.reason}</td>
                        <td>{new Date(report.createdAt).toLocaleString('fr')}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    )
};

export default Reports;