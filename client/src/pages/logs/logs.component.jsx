import React, {useEffect} from 'react';

import {useNavigate} from 'react-router-dom';

import './logs.styles.css';
import useLogs from "../../hooks/useLogs";
import {httpGetLogs} from "../../hooks/requests";
import useUserInfo from "../../hooks/useUserInfo";
const queryParams = new URLSearchParams(window.location.search);

const Logs = () => {
    const navigate = useNavigate();
    var isLoggedIn = !!localStorage.getItem('cpe-user-token');
    if (!isLoggedIn) {
        useEffect(() => {
            navigate('/login');
        });
    }
    var user = useUserInfo();
    if (user.length === undefined) {
        user = user.user;

        if (user.role === "ROLE_USER") {
            return navigate("/profile");
        }
    }

    var logTypeInit = queryParams.get("logType") ? queryParams.get("logType") : "info";
    var startDateInit = queryParams.get("startDate") ? new Date(queryParams.get("startDate")).toLocaleString('fr') : null;
    var endDateInit = queryParams.get("endDate") ? new Date(queryParams.get("endDate")).toLocaleString('fr') : null;

    var data = useLogs(logTypeInit, startDateInit, endDateInit);
    if (data.length === undefined) {
        data = data.logs;
    }

    /*const navigateToHomepage = () => {
        navigate('/');
    };*/

    return (
        <div id="logs-section" className="text-center">
            <div style={{float: "left"}}>
                <a className="btn btn-warning" href="/profile">Profile</a>
                <a className="btn btn-info" href="/analytics">Analytics</a>
                <a className="btn btn-success" href="/reports">Reports</a>
            </div>
            <h1>Logs table</h1>
            <form onSubmit={this}>
                <label htmlFor="logType">Log type</label>
                <div id="filters">
                    <div id="divLogType">
                        <select name="logType" id="logType">
                            <option value="info">Infos</option>
                            <option value="error">Errors</option>
                        </select>
                    </div>
                    <div id="divDates" style={{marginTop: "10px"}}>
                        <label>Between dates</label><br/>
                        <input type="date" id="startDate" name="startDate"/>
                        <input type="date" id="endDate" name="endDate"/>
                    </div>
                </div>
                <br/>
                <input type="submit" value="Submit filters"/>
            </form>
            <p><br/>Résultats pour les filtres : <b>{logTypeInit}</b> - <b>{startDateInit}</b> - <b>{endDateInit}</b></p>
            <table className="table table-responsive">
                <thead>
                <tr>
                    <th style={{textAlign: "center"}}>Type</th>
                    <th style={{textAlign: "center"}}>Message</th>
                    <th style={{textAlign: "center"}}>Date</th>
                </tr>
                </thead>
                <tbody>
                {data.map(log => (
                    <tr key={Math.random().toString(36).substr(2, 9)}>
                        <td>{log.type}</td>
                        <td>{log.message}</td>
                        <td>{log.createdAt}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    )

};

export default Logs;