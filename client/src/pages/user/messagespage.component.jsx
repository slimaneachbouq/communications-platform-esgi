import React, { useCallback, useEffect, useState, useRef } from "react";
import { httpGetConversations, httpGetMessages, httpSearchFriends, httpCreateMessage, httpGetCurrentUser } from '../../hooks/requests';
import Moment from 'moment';
import { useNavigate } from 'react-router-dom';

import {io} from 'socket.io-client';

import './messagespage.styles.css';

const MessagesPage = () => {
    const [currentUser, setCurrentUser] = useState();
    const [conversations, setConversations] = useState([]);
    const [chatUser, setChatUser] = useState();
    const [messages, setMessages] = useState();
    const [messagesStyle, setMessagesStyle] = useState({
      marginTop: "50vh"
    });
    const currentUserRef = useRef();
    const chatUserRef = useRef();
    const fetchConversations = async () => {
      const response = await httpGetConversations();

      if (response.status === 200) {
        const body = await response.json();
        setConversations(body);
      }
    }

    const navigate = useNavigate();
    const navigateToProfile = () => {
      navigate('/profile');
    };

    const getCurrentUser = async () => {
      const response = await httpGetCurrentUser();

      if (response.status === 200) {
        const body = await response.json();
        setCurrentUser(body);
        currentUserRef.current = body;
      }
    }

    const getLastMessage = (user) => {
      try {
        const lastMessage = (new Date(user.messageSender[0]?.createdAt) > new Date(user.messageReceiver[0]?.createdAt)) ?
        user.messageSender[0].content : user.messageReceiver[0].content;
        return lastMessage;
      } catch(err) {
        return '';
      }
    }

    const loadMessages = async (friendId) => {
      const response = await httpGetMessages(friendId);
      
      if (response.status === 200) {
        const body = await response.json();
        setMessages(body);
      }
    }

    const setConversationMessages = (friend) => {
      console.log(friend);
      setChatUser(friend);
      setMessagesStyle({});
      chatUserRef.current = friend;
      loadMessages(friend.id);
    }

    const handleSearchChange = async (e) => {
      if (e.target.value.length > 0) {
        const response = await httpSearchFriends(e.target.value);
        if (response.status === 200) {
          const body = await response.json();
          setConversations(body);
        }
      } else {
        fetchConversations();
      }
    }

    const handleMobileMenu = () => {
      document.getElementsByClassName('ms-menu')[0].classList.toggle('toggled');
    }

    useEffect(() => {
      fetchConversations();
      getCurrentUser();
    }, []);

    // sockets
    const socket = useRef();
    useEffect(() => {
      socket.current = io("http://localhost:8000");

      socket.current.on("connection", () => {
        console.log('connected to server')
      });
      socket.current.on("messageCreated", (createdMessage) => {

        const message = JSON.parse(createdMessage);
        if (currentUserRef.current?.id === message.receiver && chatUserRef.current?.id === message.sender) {
          setMessages(function(messages) {
            const isFound = messages.some(el => {
              return el.id === message.id ? true : false;
            })
            return isFound ? [...messages] : [...messages, message];
          })
        }
      })
    }, []);

    const sendMessage = async () => {
      const message = document.getElementById('message').value;
      document.getElementById('message').value = '';
      const data = {
        receiver: chatUser.id,
        content: message
      };
      const response = await httpCreateMessage(data);
      if (response.status === 200) {
        const createdMessage = await response.json();
        setMessages((messages) => [...messages, createdMessage]);
        socket.current.emit("message", JSON.stringify(createdMessage));
      }
    }
    // end sockets
    
    return (
<>
  <link
    href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet"
  />
  <div className="container bootstrap snippets bootdey mt-8">
    <div className="tile tile-alt" id="messages-main">
      <div className="ms-menu">
        <div className="ms-user clearfix">
          <img
            src="https://bootdey.com/img/Content/avatar/avatar1.png"
            alt=""
            className="img-avatar pull-left"
          />
          <div>
            Signed in as <br /> {currentUser?.email}
          </div>
        </div>
        
        <div className="p-15">
          <input type="text" className="form-control" placeholder="Search friend" onChange={handleSearchChange} />
        </div>
        <div className="list-group lg-alt">
        {conversations.map((user) => (
            <a key={user.id} className="list-group-item media" onClick={() => setConversationMessages(user)}>
              <div className="pull-left">
                <img
                  src="https://bootdey.com/img/Content/avatar/avatar1.png"
                  alt=""
                  className="img-avatar"
                />
              </div>
              <div className="media-body">
                <div className="list-group-item-heading">{user.firstName} {user.lastName}</div>
                <small className="list-group-item-text c-gray">
                  {getLastMessage(user)}
                </small>
              </div>
            </a>
          ))}
        </div>
      </div>
      <div className="ms-body">
        <div className="action-header clearfix">
          <div className="visible-xs" id="ms-menu-trigger" onClick={handleMobileMenu}>
            <i className="fa fa-bars" />
          </div>
          { chatUser && 
            <div className="pull-left hidden-xs">
              <img
                src="https://bootdey.com/img/Content/avatar/avatar1.png"
                alt=""
                className="img-avatar m-r-10"
              />
              <div className="lv-avatar pull-left"></div>
              <span>{chatUser.firstName} {chatUser.lastName}</span>
            </div>
          }

            <ul className="ah-actions actions">
                    <li>
                        <a onClick={navigateToProfile}>
                            <i className="glyphicon glyphicon-user"></i>
                        </a>
                    </li>
              </ul>

        </div>
        {messages && messages.map((message) => (
          <div key={message.id} className={(() => {
              if (message.sender == chatUser.id) {
                return 'message-feed media';
              } else {
                return 'message-feed right';
              }
            })()}>
              <div className={(() => {
              if (message.sender == chatUser.id) {
                return 'pull-left';
              } else {
                return'pull-right';
              }
              })()}>
                <img
                  src="https://bootdey.com/img/Content/avatar/avatar1.png"
                  alt=""
                  className="img-avatar"
                />
              </div>
              <div className="media-body">
                <div className="mf-content">
                  {message.content}
                </div>
                <small className="mf-date">
                  <i className="fa fa-clock-o" /> {Moment(message.createdAt).format("MMM Do YY, HH:mm")}
                </small>
              </div>
          </div>
        ))}
        <div className="msb-reply" style={messagesStyle}>
            <textarea id="message" placeholder="What's on your mind..." defaultValue={""} />
            <button onClick={sendMessage}>
              <i className="fa fa-paper-plane-o" />
            </button>
        </div>
      </div>
    </div>
  </div>
</>
    )
};

export default MessagesPage;