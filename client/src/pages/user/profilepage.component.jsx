import React, {useCallback, useEffect, useState, useRef} from 'react';

import ProfileBox from '../../components/user/profile-box/profile-box.component';
import ProfileInfo from '../../components/user/profile-info/profile-info.component';
import {useCurrentUser} from "../../contexts/CurrentUserContext";

import './profilepage.styles.css';
import useUserInfo from "../../hooks/useUserInfo";
import {useNavigate} from "react-router-dom";

const ProfilePage = () => {
    const {currentUser, fetchCurrentUser} = useCurrentUser();
    useEffect(() => {
        fetchCurrentUser();
    }, []);
    return (
        <>
            <link
                href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
                rel="stylesheet"
            />
            <div className="container bootstrap snippets bootdeys mt-8">
                <div className="row" id="user-profile">
                    <ProfileBox currentUser={currentUser?.user}/>
                    <ProfileInfo currentUser={currentUser?.user}/>
                </div>
            </div>
        </>
    )
};

export default ProfilePage;