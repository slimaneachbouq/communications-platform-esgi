import React, {useEffect} from 'react';

import {useNavigate} from 'react-router-dom';

import './analytics.styles.css';
import {httpGetAnalytics} from "../../hooks/requests";
import useAnalytics from "../../hooks/useAnalytics";
import useUserInfo from "../../hooks/useUserInfo";

const Analytics = () => {
    const navigate = useNavigate();
    var isLoggedIn = !!localStorage.getItem('cpe-user-token');
    if (!isLoggedIn) {
        useEffect(() => {
            navigate('/login');
        });
    }
    const data = useAnalytics();
    var user = useUserInfo();
    if (user.length === undefined) {
        user = user.user;

        if (user.role === "ROLE_USER") {
            return navigate("/profile");
        }
    }
    /*const navigateToHomepage = () => {
        navigate('/');
    };*/

    return (
        <div id="analytics-section" className="text-center">
            <div style={{float: "left"}}>
                <a className="btn btn-warning" href="/profile">Profile</a>
                <a className="btn btn-info" href="/logs">Logs</a>
                <a className="btn btn-danger" href="/reports">Reports</a>
            </div>
            <h1>Analytics information</h1>
            <div className="row">
                <div id="totalUsers">
                    <h2>Total utilisateurs inscrits</h2>
                    <p>{data.totalUsers}</p>
                </div>
                <div id="dureeSession">
                    <h2>Durée de session moyenne</h2>
                    <p>{data.totalMinutes} minutes</p>
                </div>
            </div>

            <div className="row">
                <div id="tabAppareils">
                    <h2>Appareils utilisés</h2>
                    <p></p>
                </div>
                <div id="nbVisiteurs">
                    <h2>Nombre de visiteurs (en temps réel)</h2>
                    <p></p>
                </div>
            </div>
        </div>

    )
};

export default Analytics;