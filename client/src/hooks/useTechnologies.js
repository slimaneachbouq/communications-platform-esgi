import { useCallback, useEffect, useState } from "react";

import { httpGetTechnologies } from "./requests";

function useTechnologies() {
    const [technologies, saveTechnologies] = useState([]);

    const getAllTechnologies = useCallback(async () => {
        const fetchedTechnologies = await httpGetTechnologies();
        // console.log(fetchedTechnologies);
        let technologiesArray = fetchedTechnologies.map((val, _) => {
            return {label:val.title, value:val.id};
        })
        saveTechnologies(technologiesArray);
    }, []);

    useEffect(() => {
        getAllTechnologies();
    }, [getAllTechnologies]);

    return technologies;
}

export default useTechnologies;