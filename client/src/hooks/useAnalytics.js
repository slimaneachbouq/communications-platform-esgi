import { useCallback, useEffect, useState } from "react";

import { httpGetAnalytics } from "./requests";

function useAnalytics() {
    const [analytics, saveAnalytics] = useState([]);

    const getAllAnalytics = useCallback(async () => {
        const fetchedAnalytics = await httpGetAnalytics();
        saveAnalytics(fetchedAnalytics);
    }, []);

    useEffect(() => {
        getAllAnalytics();
    }, [getAllAnalytics]);

    return analytics;
}

export default useAnalytics;