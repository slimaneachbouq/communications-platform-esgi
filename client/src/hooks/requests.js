import useUserInfo from "./useUserInfo";

const API_URL = 'http://localhost:8000/v1';

async function httpGetTechnologies() {
    const response = await fetch(`${API_URL}/technologies`);
    return await response.json();
}

async function httpGetAnalytics() {
    const response = await fetch(`${API_URL}/analytics`);
    return await response.json();
}

async function httpGetLogs(logType, startDate, endDate) {
    return await fetch(`${API_URL}/logs`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({'logType': logType, 'startDate': startDate, 'endDate': endDate})
    });
}

async function getFriends() {
    try {
        const jwt = localStorage.getItem('cpe-user-token');
        return await fetch(`${API_URL}/user-friends`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
        });

    } catch (error) {
        console.error(error);
    }
}

async function httpGetInvitations() {
    try {
        const jwt = localStorage.getItem('cpe-user-token');
        return await fetch(`${API_URL}//user-invitations`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
        });
    } catch (error) {
        console.error(error);
    }
}

async function httpSignup(userData) {
    try {
        // console.log(JSON.stringify(userData));
        return await fetch(`${API_URL}/signup`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(userData)
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpGetReports() {
    try {
        const response = await fetch(`${API_URL}/reports`);
        return await response.json();
    } catch (err) {
        console.log(err);
        // logger.error(err);
        return {
            ok: false,
        }
    }
}

async function httpCreateReport(createdBy, userReported, reason) {
    const jwt = localStorage.getItem('cpe-user-token');

    try {
        return await fetch(`${API_URL}/new-report/new`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
            method: "POST",
            body: JSON.stringify({createdBy, userReported, reason})
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpLoginVerification(jwt, authLinkId) {
    try {
        return await fetch(`${API_URL}/signin-verification`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
            body: JSON.stringify({'auth_link': authLinkId})
        });
    } catch (err) {
        console.error(err);
        return {
            ok: false,
        }
    }
}

async function httpGetUserInfo() {
    const jwt = localStorage.getItem('cpe-user-token');
    try {
        return await fetch(`${API_URL}/user-info`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
            // body: JSON.stringify({'auth_link' : authLinkId})
        });
    } catch (err) {
        console.error(err);
        return {
            ok: false,
        }
    }
}

async function httpSignin(data) {
    try {
        return await fetch(`${API_URL}/signin`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        });
    } catch (err) {
        console.error(err);
        return {
            ok: false
        }
    }
}

async function httpLogout() {
    try {
        return await fetch(`${API_URL}/logout`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({'logout': true, 'email': localStorage.getItem('user-mail')})
        });
    } catch (err) {
        console.error(err);
        return {
            ok: false
        }
    }
}

async function httpGetUsersNotRelatedToAuthUser() {
    try {
        const jwt = localStorage.getItem('cpe-user-token');
        return await fetch(`${API_URL}/users`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpGetConversations() {
    try {
        const jwt = localStorage.getItem('cpe-user-token');
        return await fetch(`${API_URL}/user-conversations`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpGetMessages(friendId) {
    try {
        const jwt = localStorage.getItem('cpe-user-token');
        return await fetch(`${API_URL}/user-messages/${friendId}`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpSearchFriends(value) {
    try {
        const jwt = localStorage.getItem('cpe-user-token');
        return await fetch(`${API_URL}/user-search-friends/${value}`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpCreateMessage(data) {
    try {
        const jwt = localStorage.getItem('cpe-user-token');
        return await fetch(`${API_URL}/messages`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
            body: JSON.stringify(data)
        });
    } catch (err) {
        console.error(err);
        return {
            ok: false
        }
    }
}

async function httpGetCurrentUser() {
    try {
        const jwt = localStorage.getItem('cpe-user-token');
        return await fetch(`${API_URL}/authenticated-user`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpCreateInvitation(receiver) {
    const jwt = localStorage.getItem('cpe-user-token');
    try {
        return await fetch(`${API_URL}/new-invitation`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
            method: "POST",
            body: JSON.stringify({jwt, receiver})
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpUpdateInvitation(sender, choice) {
    const jwt = localStorage.getItem('cpe-user-token');
    try {
        return await fetch(`${API_URL}/update-invitation`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
            method: "POST",
            body: JSON.stringify({sender, jwt, choice})
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpRemoveInvitation(friend) {
    const jwt = localStorage.getItem('cpe-user-token');
    try {
        return await fetch(`${API_URL}/remove-invitation`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
            method: "POST",
            body: JSON.stringify({friend, jwt})
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

async function httpResetPassword(data) {
    try {
        return await fetch(`${API_URL}/reset-password`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        });
    } catch (err) {
        console.error(err);
        return {
            ok: false
        }
    }
}

async function httpResetPasswordConfirmation(data) {
    console.log(data);
    try {
        return await fetch(`${API_URL}/reset-password-confirmation`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        });
    } catch (err) {
        console.error(err);
        return {
            ok: false
        }
    }
}

async function httpUpdateProfileInfos(infos) {
    const jwt = localStorage.getItem('cpe-user-token');
    try {
        return await fetch(`${API_URL}/update-infos`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${jwt}`,
            },
            method: "POST",
            body: JSON.stringify(infos)
        });
    } catch (err) {
        return {
            ok: false,
        }
    }
}

export {
    httpGetTechnologies,
    httpSignup,
    httpGetAnalytics,
    httpLoginVerification,
    httpLogout,
    httpSignin,
    httpGetUsersNotRelatedToAuthUser,
    httpResetPassword,
    httpResetPasswordConfirmation,
    httpGetUserInfo,
    httpGetLogs,
    getFriends,
    httpGetInvitations,
    httpCreateInvitation,
    httpUpdateInvitation,
    httpRemoveInvitation,
    httpGetConversations,
    httpGetMessages,
    httpSearchFriends,
    httpCreateMessage,
    httpGetCurrentUser,
    httpGetReports,
    httpCreateReport,
    httpUpdateProfileInfos
}