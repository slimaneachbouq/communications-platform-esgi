import { useCallback, useEffect, useState } from "react";

import { httpGetLogs } from "./requests";

function useLogs(logType = "", startDate = null, endDate = null) {
    const [logs, saveLogs] = useState([]);

    const getAllLogs = useCallback(async () => {
        const fetchedLogs = await httpGetLogs(logType, startDate, endDate);
        saveLogs(await fetchedLogs.json());
    }, []);

    useEffect(() => {
        getAllLogs();
    }, [getAllLogs]);

    return logs;
}

export default useLogs;