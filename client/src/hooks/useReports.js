import {useCallback, useEffect, useState} from "react";

import {httpCreateReport, httpGetReports} from './requests';

function useReport() {
    const [reports, saveReports] = useState([]);

    const getAllReports = useCallback(async () => {
        const fetchedReports = await httpGetReports();
        saveReports(await fetchedReports);
    }, []);

    useEffect(() => {
        getAllReports();
    }, []);

    return reports;
}

export function createReport(createdBy, userReported, reason) {
    console.log("createReport REACT")
    const newReport = httpCreateReport(createdBy, userReported, reason);

    return newReport;
}

export default useReport;