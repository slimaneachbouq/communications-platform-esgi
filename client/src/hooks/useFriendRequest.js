import {useCallback, useEffect, useState} from "react";

import {httpCreateInvitation, httpRemoveInvitation, httpUpdateInvitation} from './requests';


function createFriendRequest(receiver) {
    return httpCreateInvitation(receiver);
}

export function updateFriendRequest(sender, choice) {
    return httpUpdateInvitation(sender, choice);
}

export function removeFriendRequest(sender) {
    return httpRemoveInvitation(sender);
}

export default createFriendRequest;