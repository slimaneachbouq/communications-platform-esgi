import React from 'react';
import {BrowserRouter, Routes, Route, Navigate, Router, Redirect} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';


import ProfilePage from './pages/user/profilepage.component';
import MessagesPage from './pages/user/messagespage.component';
import LoginPage from './pages/loginpage.component';
import SignUp from './pages/sign-up.component';
import Analytics from './pages/analytics/analytics.component';
import LoginVerificationPage from './pages/loginverificationpage.component';
import Logout from "./pages/logout.component";
import Logs from "./pages/logs/logs.component";
import Reports from "./pages/reports/reports.component";
import ResetPassword from './pages/reset-password.component';
import InputResetPassword from './pages/input-reset-password.component';
import {CurrentUserProvider} from "./contexts/CurrentUserContext";

function App() {
    return (
        <CurrentUserProvider>
            <BrowserRouter>
                <Routes>
                    <Route exact path="/profile" element={<ProfilePage/>}></Route>
                    <Route exact path="/messages" element={<MessagesPage/>}></Route>
                    <Route exact path="/login" element={<LoginPage/>}></Route>
                    <Route exact path="/signup" element={<SignUp/>}></Route>
                    <Route exact path="/analytics" element={<Analytics/>}></Route>
                    <Route exact path="/login-verification/:jwt/:id" element={<LoginVerificationPage/>}></Route>
                    <Route exact path="/logout" element={<Logout/>}></Route>
                    <Route exact path="/logs" element={<Logs/>}></Route>
                    <Route exact path="/reset-password" element={<ResetPassword/>}></Route>
                    <Route exact path="/reset-password-confirmation/:token" element={<InputResetPassword/>}></Route>
                    <Route exact path="/reports" element={<Reports/>}></Route>
                </Routes>
            </BrowserRouter>
        </CurrentUserProvider>
    );
}

export default App;
