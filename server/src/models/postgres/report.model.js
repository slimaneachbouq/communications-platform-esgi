const {userReports, report} = require('./index');
const logger = require('../../services/logger');

async function saveReport(data) {
    return report.create({
        createdBy: data[0],
        userReported: data[1],
        reason: data[2],
        createdAt: new Date(),
    });
}

async function getReports() {
    const data = {
        "reports": await report.findAll()
    }
    return data;
}

module.exports = {
    saveReport,
    getReports
};