const { user, authLink } = require('./index');
const logger = require('../../services/logger');

async function createAuthLink(userId) {
    return await authLink.create({
        userId: userId
    });
}


module.exports = {
    createAuthLink
};