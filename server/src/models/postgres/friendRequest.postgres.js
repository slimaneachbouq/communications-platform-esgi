const { Model, DataTypes } = require("sequelize");
const connection = require("../../services/postgres");
class FriendRequest extends Model{}

FriendRequest.init({
    status: {
        type: DataTypes.BOOLEAN,
        defaultValue: null,
      },
    },
    {
        sequelize:connection,
        modelName: "friend_request",
    }
);

module.exports = FriendRequest;