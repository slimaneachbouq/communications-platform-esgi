const { message } = require('./index');
const logger = require('../../services/logger');

async function createMessage(data) {
    return await message.create(data);
}

module.exports = {
    createMessage
};