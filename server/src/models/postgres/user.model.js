var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;
const {callback} = require("pg/lib/native/query");
const {user, technology, userTechnologies, message, userReports} = require('./index');
const logger = require('../../services/logger');
const sequelize = require('../../services/postgres');
const {QueryTypes} = require('sequelize');
const userDatabase = require("./user.postgres");

async function saveUser(data) {
    return await user.create(
        data,
        {
            include: [
                {
                    model: userTechnologies,
                    as: 'user_technologies'
                }
            ]
        });
}

var totalMinutes = null;

async function getAllTotalUsers() {
    var allSessions = null;

    function getAll() {
        return new Promise(function (resolve, reject) {
            MongoClient.connect(process.env.MONGO_URL).then(function (db) {
                var dbo = db.db("chatapp");

                dbo.collection('DureeSession').find({email: "duree_session_totale_6a934b454c61d9b8"}).toArray().then(async (res) => {
                    resolve(res);
                });
            })
        });
    }

    let time = getAll().then(function (result) {
        allSessions = result;

        return allSessions[0].totalMinutes;
    });

    const data = {
        "totalUsers": (await userDatabase.findAll()).length,
        "totalMinutes": round(await time, 2)
    }
    return data;
}

const round = (n, dp) => {
    const h = +('1'.padEnd(dp + 1, '0')) // 10 or 100 or 1000 or etc
    return Math.round(n * h) / h
}

async function getUserInfo(id) {
    const data = {
        "user": await userDatabase.findOne({
            where: {
                id: id
            },
            include: [
                {
                    model: technology,
                    as: 'technologies'
                }
            ]
        }),
    }
    return data;
}

async function updateUser(userId, data) {
    return await user.update(
        data,
        {
            where: {id: userId},
            individualHooks: true,
        },
        {
            include: [
                {
                    model: userTechnologies,
                    as: 'user_technologies'
                }
            ]
        }
    )
}

async function getConversations(userId) {
    const senders = await sequelize.query("SELECT DISTINCT sender FROM messages WHERE receiver = ?",
        {
            model: message,
            replacements: [userId],
            type: QueryTypes.SELECT
        });
    const sendersId = senders.map(obj => obj.sender);

    const receivers = await sequelize.query("SELECT DISTINCT receiver FROM messages WHERE sender = ?",
        {
            model: message,
            replacements: [userId],
            type: QueryTypes.SELECT
        });
    const receiversId = receivers.map(obj => obj.receiver);
    const friends = sendersId.concat(receiversId.filter((element) => sendersId.indexOf(element) < 0));

    return await user.findAll({
        where: {
            id: friends,
        },
        order: [
            [{model: message, as: 'messageSender'}, 'createdAt', 'DESC'],
            [{model: message, as: 'messageSender'}, 'createdAt', 'DESC']
        ],
        include: [
            {
                model: message,
                as: 'messageSender',
            },
            {
                model: message,
                as: 'messageReceiver',
            }
        ]
    });
}

async function getMessages(userId, friendId) {
    const messages = await sequelize.query(
        "SELECT * FROM messages WHERE (receiver = " + userId + " AND sender = " + friendId + " ) OR (receiver = " + friendId + " AND sender = " + userId + ")",
        {
            model: message,
            type: QueryTypes.SELECT
        });

    return messages;
}

module.exports = {
    getAllTotalUsers,
    saveUser,
    getUserInfo,
    getConversations,
    getMessages,
    updateUser
};