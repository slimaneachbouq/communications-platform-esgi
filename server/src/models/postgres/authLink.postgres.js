const { Model, DataTypes } = require("sequelize");
const connection = require("../../services/postgres");
class authLink extends Model{}

authLink.init({},
    {
        sequelize:connection,
        modelName: "auth_link",
    }
);

module.exports = authLink;