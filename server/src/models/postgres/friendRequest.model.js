const { user, friendRequest } = require('./index');
const logger = require('../../services/logger');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

async function getUsersNotRelatedWithTheGivenUser(userId) {
    const users = await user.findAll({
        where : {
            id : {[Op.ne] : userId},
            "$friendRequestReceiver.sender$": {
                [Op.or] : {
                    [Op.ne] : userId,
                    [Op.eq]: null
                }
            },
            "$friendRequestReceiver.receiver$": {
                [Op.or] : {
                    [Op.ne] : userId,
                    [Op.eq]: null
                }
            },
            "$friendRequestSender.sender$": {
                [Op.or]: {
                    [Op.ne]: userId,
                    [Op.eq]: null
                }
            },
            "$friendRequestSender.receiver$": {
                [Op.or]: {
                    [Op.ne]: userId,
                    [Op.eq]: null
                }
            }
        },
        include: [
            { 
                model: friendRequest,
                as: 'friendRequestSender',
                required: false
            },
            { 
                model: friendRequest,
                as: 'friendRequestReceiver',
                required: false 
            }
        ]
    });

    return users;
}

async function getFriends(userId) {
    const users = await user.findAll({
        where : {
            id : {[Op.ne] : userId},
            [Op.or] : {
                "$friendRequestSender.receiver$": userId,
                "$friendRequestSender.sender$": userId,
                "$friendRequestReceiver.sender$": userId,
                "$friendRequestReceiver.receiver$": userId,
            },
            [Op.or] : {
                "$friendRequestSender.status$": true,
                "$friendRequestReceiver.status$": true
            }
            
        },
        include: [
            { 
                model: friendRequest,
                as: 'friendRequestSender',
                required: false
            },
            { 
                model: friendRequest,
                as: 'friendRequestReceiver',
                required: false 
            }
        ]
    });

    return users;
}


async function getInvitations(userId) {
    const invitations = await friendRequest.findAll(
        {
            where:
                {
                    receiver: userId,
                    status: null
                },
            include: {
                model: user,
                as: 'userIdSender'
            }
        }
    );
    return invitations;


}

async function saveFriendRequest(data) {
    return friendRequest.create({
        sender: data[0],
        receiver: data[1],
        status: null,
        createdAt: new Date(),
    });
}

async function updateFriendRequest(data) {
    if (data[2]) {
        return friendRequest.update(
            {
                status: data[2],
                updatedAt: new Date(),
            },
            {
                where: {
                    sender: data[0],
                    receiver: data[1],
                }
            }
        );
    }

    let friend = data[0];
    let loggedUser = data[1];

    return friendRequest.destroy(
        {
            where: {
                [Op.or]: [
                    {
                        sender: friend,
                        receiver: loggedUser,
                    },
                    {
                        sender: loggedUser,
                        receiver: friend,
                    }
                ]
            }
        }
    );
}

async function removeFriendRequest(data) {
    let friend = data[0];
    let loggedUser = data[1];

    return friendRequest.destroy(
        {
            where: {
                [Op.or]: [
                    {
                        sender: friend,
                        receiver: loggedUser,
                        status: true
                    },
                    {
                        sender: loggedUser,
                        receiver: friend,
                        status: true
                    }
                ]
            }
        }
    );
}


async function searchFriends(userId, value) {
    const users = await user.findAll({
        where : {
            id : {[Op.ne] : userId},
            [Op.and] : {
                [Op.or] : {
                    firstName: { [Op.like]: `%${value}%` },
                    lastName: { [Op.like]: `%${value}%` },
                },
            },
            [Op.or] : {
                "$friendRequestSender.receiver$": userId,
                "$friendRequestSender.sender$": userId,
                "$friendRequestReceiver.sender$": userId,
                "$friendRequestReceiver.receiver$": userId,
            },
            [Op.or] : {
                "$friendRequestSender.status$": true,
                "$friendRequestReceiver.status$": true
            }

        },
        include: [
            {
                model: friendRequest,
                as: 'friendRequestSender',
                required: false
            },
            {
                model: friendRequest,
                as: 'friendRequestReceiver',
                required: false
            }
        ]
    });

    return users;
}

module.exports = {
    getUsersNotRelatedWithTheGivenUser,
    getFriends,
    getInvitations,
    saveFriendRequest,
    updateFriendRequest,
    removeFriendRequest,
    getFriends,
    searchFriends
};