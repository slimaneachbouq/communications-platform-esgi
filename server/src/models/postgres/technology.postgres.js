const { Model, DataTypes } = require("sequelize");
const connection = require("../../services/postgres");

class Technology extends Model {}

Technology.init({
    title: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
},
{
    sequelize: connection,
    modelName: "technology",
});

module.exports = Technology;