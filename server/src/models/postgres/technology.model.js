const technologyDatabase = require('./technology.postgres');

const technologiesFixtures = [
    'JavaScript',
    'PHP',
    'Ruby',
    'Java',
    'Swift',
    'C#',
    'C++',
    'C',
    'Python',
    'Julia',
    'Scala'
];

async function loadTechnologiesData() {
    try {
        technologiesFixtures.forEach(async (technology) => await saveTechnology(technology));
    } catch(err) {
        console.log(err);
    }
};

async function saveTechnology(title) {
    try {
        await technologyDatabase.upsert({
            title
        });
    } catch(err) {
        console.log(err);
    }
};

async function getAllTechnologies() {
    return await technologyDatabase.findAll();
}

module.exports = {
    loadTechnologiesData,
    getAllTechnologies
};