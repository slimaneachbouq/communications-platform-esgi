const { Model, DataTypes } = require("sequelize");
const connection = require("../../services/postgres");

class UserTechnology extends Model {}

UserTechnology.init({},
{
    sequelize: connection,
    modelName: "user_technologies",
    timestamps: false
});

module.exports = UserTechnology;