const {Model, DataTypes} = require("sequelize");
const connection = require("../../services/postgres");
const bcryptjs = require("bcryptjs");
const logger = require('../../services/logger');

class Report extends Model {
}

Report.init({
        createdBy: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: {
                    min: 8,
                    max: 255,
                },
            },
        },
        userReported: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: {
                    min: 8,
                    max: 255,
                },
            },
        },
        reason: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: {
                    min: 1,
                    max: 255,
                },
            },
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
    },
    {
        sequelize: connection,
        modelName: "report",
    }
)

module.exports = Report;
