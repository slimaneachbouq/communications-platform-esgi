// const { sequelize } = require("sequelize");
exports.connection = require("../../services/postgres");
exports.user  = require("./user.postgres");
exports.report  = require("./report.postgres");
exports.message = require('./message.postgres');
exports.technology = require('./technology.postgres');
exports.friendRequest = require('./friendRequest.postgres');
exports.userTechnologies = require('./userTechnology.postgres');
exports.authLink = require('./authLink.postgres');
exports.message = require('./message.postgres');

exports.user.hasMany(exports.friendRequest, { as: 'friendRequestSender', foreignKey: 'sender' });
exports.friendRequest.belongsTo(exports.user, { as: 'userIdSender', foreignKey: 'sender' });

exports.user.hasMany(exports.friendRequest, { as: 'friendRequestReceiver', foreignKey: 'receiver' });
exports.friendRequest.belongsTo(exports.user, { as: 'userIdReceiver', foreignKey: 'receiver' });

exports.user.belongsToMany(exports.technology, {
    as : "technologies",
    through: 'user_technologies',
    foreignKey: 'user_id',
});
exports.technology.belongsToMany(exports.user, {
    as : 'users',
    through: 'user_technologies',
    foreignKey: 'technology_id',
});
exports.user.hasMany(exports.userTechnologies, { as: 'user_technologies', foreignKey: 'user_id'} );

exports.user.hasMany(exports.authLink, { foreignKey: 'userId'});
exports.authLink.belongsTo(exports.user, { foreignKey: 'userId'});

exports.user.hasMany(exports.message, {
    as : 'messageSender',
    foreignKey: 'sender'
});
exports.message.belongsTo(exports.user, { as: 'senderId', foreignKey: 'sender' });
exports.user.hasMany(exports.message, {
    as : 'messageReceiver',
    foreignKey: 'receiver'
});
exports.message.belongsTo(exports.user, { as: 'receiverId', foreignKey: 'receiver' });
