const logger = require('../../services/logger');
var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;

async function getAllLogs(logType, startDate, endDate) {
    var allLogs = null;
    function getAll() {
        return new Promise(function (resolve, reject) {
            MongoClient.connect(process.env.MONGO_URL).then(function (db) {
                var dbo = db.db("chatapp");

                if (logType !== "" && startDate && endDate) {
                    console.log("ici 1");
                    console.log(startDate, ", ", endDate);
                    dbo.collection('Logs').find(
                        {
                            type: logType,
                            createdAt: {
                                $gte: startDate,
                                $lte: endDate
                            }
                        }
                    ).toArray().then(async (res) => {
                        resolve(res);
                    });
                }

                if (logType !== "" && !startDate && !endDate) {
                    console.log("ici 2");

                    dbo.collection('Logs').find(
                        {
                            type: logType,
                        }
                    ).toArray().then(async (res) => {
                        resolve(res);
                    });
                }

                if (logType === "" && !startDate && !endDate) {
                    dbo.collection('Logs').find().toArray().then(async (res) => {
                        resolve(res);
                    });
                }
            })
        });
    }

    let logs = getAll().then(function (result) {
        allLogs = result;
        return allLogs;
    });

    const data = {
        "logs": await logs,
    }
    return data;
}

module.exports = {
    getAllLogs,
};