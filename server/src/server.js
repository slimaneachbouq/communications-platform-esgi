const http = require('http');
const io = require('socket.io');

require('dotenv').config();
const { mongoConnect } = require('./services/mongo');
const connection = require('./services/postgres');
const app = require('./app');

const { loadTechnologiesData } = require('./models/postgres/technology.model');

const PORT = process.env.PORT || 8000;

const server = http.createServer(app);
const socketServer = io(server, {
    cors: {
        origin: 'http://localhost:3000'
    }
});

const sockets = require('./services/sockets');

async function startServer() {
    await mongoConnect();
    await loadTechnologiesData();

    server.listen(PORT, () => {
        console.log(`Listening on port ${PORT}...`);
    });
    sockets.listen(socketServer);
}

startServer();