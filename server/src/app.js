const express = require('express');
const cors = require('cors');
var session = require('express-session');
const sessionStore = new session.MemoryStore();

const api = require('./routes/api');

const app = express();

app.use(cors({
    origin: 'http://localhost:3000'
}));

const randomString = () => {
    var length = 10;
    var id = '';
    while (id.length < length) {
        var ch = Math.random()
            .toString(36)
            .substr(2, 1);
        if (Math.random() < 0.5) {
            ch = ch.toUpperCase();
        }
        id += ch;
    }
    return id;
}

app.use(session({
    secret: randomString(),
    cookie: {maxAge: 60000},
    saveUninitialized: false,
    sessionStore
}));

app.post('/login', (req, res) => {
    res.send('ok');
});

app.use(express.json());

app.use('/v1', api);



module.exports = app;