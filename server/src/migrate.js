const { connection } = require("./models/postgres");
const logger = require('./services/logger');

connection
  .sync({ force: true })
  .then(() => {
    console.info("Database synced");
  })
  .catch((err) => {
    console.error(err);
  })
  .finally(() => {
    connection.close();
  });