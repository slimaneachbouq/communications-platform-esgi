const io = require('socket.io');

function listen(io) {
    io.on('connection', (socket) => {
        console.log('a user connected !', socket.id);

        socket.on('message', (message) => {
            console.log(`message from ${socket.id} : ${message}`);
            socket.broadcast.emit('messageCreated', message);
        });

        socket.on('disconnect', () => {
            console.log(`socket ${socket.id} disconnect`)
        })
    });

};

module.exports = {
    listen
}