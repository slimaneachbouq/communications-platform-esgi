const Sequelize = require("sequelize");
const logger = require('./logger');

require('dotenv').config();

console.log(process.env.POSTGRES_URL);

const connection = new Sequelize(process.env.POSTGRES_URL, {
  useNewUrlParser: true,
});
connection.authenticate().then(() => {
  logger.info("Postgres connection ready!");
});

module.exports = connection;