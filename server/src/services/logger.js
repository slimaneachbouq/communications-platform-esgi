const { createLogger, format, transports } = require('winston');
const path = require('path');

const logger = createLogger({
    level: 'info',
    exitOnError: false,
    format: format.json(),
    transports: [
      new transports.File({ filename: path.join('logs', 'out.log')}),
    ],
  });
  
module.exports = logger;

// exports.readLog = () => {
//   let log = fs.readFileSync('../../logs/out.log','utf8', 
//     (error, content) => {
//       if(error) {
//         log4js.getLogger().error(error);
//         return error;
//       }
//       return content;
//     });
//   return log;
// }