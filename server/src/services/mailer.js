var nodemailer = require('nodemailer');
const logger = require('./logger');
var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;

function sendEmail(to, subject, content) {
    var transport = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
            user: "297db6bf5ee0db",
            pass: "505cf22978c23b"
        }
    });

    var mailOptions = {
        from: 'esgi_iwa3@gmail.com',
        to: to,
        subject: subject,
        html: content
    }

    transport.sendMail(mailOptions, function (error, info) {
        if (error) {
            logger.error('There is an error while sending the email');

            MongoClient.connect(process.env.MONGO_URL, function (err, db) {
                var dbo = db.db("chatapp");
                var logObj = {
                    type: "error",
                    message: "A mail could not been sent to " + to + ".",
                    createdAt: new Date().toLocaleString('fr')
                };

                dbo.listCollections({name: "Logs"}).next(function (err, collinfo) {
                    if (!collinfo) {
                        dbo.createCollection("Logs", function (err, res) {
                            if (err) throw err;
                            console.log("Collection Logs created!");
                        })
                    }

                    dbo.collection("Logs").insertOne(logObj, function (err, res) {
                        if (err) throw err;
                        // db.close();
                    });

                    dbo.collection("Logs").find({}).toArray(function (err, result) {
                        if (err) throw err;
                        console.log(result);
                        db.close();
                    });
                });
            });
        } else {
            logger.info('Email sent !');

            MongoClient.connect(process.env.MONGO_URL, function (err, db) {
                var dbo = db.db("chatapp");
                var logObj = {
                    type: "info",
                    message: "A mail has been sent to " + to + ".",
                    createdAt: new Date().toLocaleString('fr')
                };

                dbo.listCollections({name: "Logs"}).next(function (err, collinfo) {
                    if (!collinfo) {
                        dbo.createCollection("Logs", function (err, res) {
                            if (err) throw err;
                            console.log("Collection Logs created!");
                        })
                    }

                    dbo.collection("Logs").insertOne(logObj, function (err, res) {
                        if (err) throw err;
                        // db.close();
                    });

                    dbo.collection("Logs").find({}).toArray(function (err, result) {
                        if (err) throw err;
                        db.close();
                    });
                });
            });
        }
    });
}

module.exports = sendEmail;