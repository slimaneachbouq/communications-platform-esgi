const express = require('express');
const { httpGetAllLogs } = require('./logs.controller');

const logsRouter = express.Router();

logsRouter.post('/', httpGetAllLogs);

module.exports = logsRouter;