const {getAllLogs} = require("../../models/mongo/logs.model");

async function httpGetAllLogs(req, res) {
    const logs = await getAllLogs(req.body.logType, req.body.startDate, req.body.endDate);
    return res.status(200).json(logs);
}

module.exports = {
    httpGetAllLogs,
};