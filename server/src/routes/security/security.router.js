const express = require('express');
const { httpSignup, httpSignin, httpVerifySigninUrl, httpResetPassword, httpResetPasswordConfirmation, httpLogout } = require('./security.controller');
const checkAuthentication = require("../../middlewares/checkAuthentication");
const {httpGetUsers} = require("../users/users.controller");

const securityRouter = express.Router();

securityRouter.post('/signup', httpSignup);
securityRouter.post('/signin', httpSignin);
securityRouter.post('/signin-verification',checkAuthentication, httpVerifySigninUrl);
securityRouter.post('/logout',httpLogout);
securityRouter.post('/reset-password', httpResetPassword);
securityRouter.post('/reset-password-confirmation', httpResetPasswordConfirmation);
securityRouter.get('/users',checkAuthentication, httpGetUsers);

module.exports = securityRouter;