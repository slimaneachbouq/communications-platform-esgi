const {saveUser} = require('../../models/postgres/user.model');
const User = require("../../models/postgres/user.postgres");
const AuthLink = require("../../models/postgres/authLink.postgres");
const {ValidationError} = require("sequelize");
const {formatError} = require('../../services/helpers');
const logger = require('../../services/logger');
const bcryptjs = require("bcryptjs");
const {createToken} = require("../../services/tokenManager");
const {createAuthLink} = require('../../models/postgres/authLink.model')
const sendEmail = require('../../services/mailer');
var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;
const userDatabase = require('../../models/postgres/user.postgres');

async function httpSignup(req, res) {
    const user = req.body;
    // console.log("user : ", user);

    if (!user.firstName || !user.lastName || !user.email || !user.password || !user.role) {
        return res.status(400).json({
            error: 'Missing required property!',
        });
    }

    try {
        const response = await saveUser(user);
        logger.info(response);
        res.status(201).json({
            message: 'ok!'
        });
    } catch (err) {
        if (err instanceof ValidationError) {
            logger.error(formatError(err));
            res.status(422).json(formatError(err));
        } else {
            logger.error(err.message);
            res.status(500).json({
                message: err.message,
            });
        }
    }
};

async function httpSignin(req, res) {
    try {
        const result = await User.findOne({where: {email: req.body.email}});
        if (
            result && bcryptjs.compareSync(req.body.password, result.password)
        ) {
            const newToken = await createToken(result);
            const authLink = await createAuthLink(result.id);
            res.json({
                token: newToken,
                message: 'Please check your email to signin !'
            });

            sendEmail(result.email, 'Login to your account !', '<a href="http://localhost:3000/login-verification/' + newToken + '/' + authLink.id +'">Confirmer la connexion</a>');
        } else {
            res.status(401).json({
                'message': 'Email or password is incorrect !'
            });
        }
    } catch (error) {
        if (error instanceof ValidationError) {
            logger.error(error.message);
            res.status(422).json({
                'message': error.message
            });
        } else {
            logger.error(error.message);
            res.status(500).json({'message': error.message});
        }
    }
};

async function httpVerifySigninUrl(req, res) {
    const nbline = await AuthLink.destroy({
        where: {
            id: req.body.auth_link,
            userId: req.user.id
        }
    });

    if (!nbline) {
        MongoClient.connect(process.env.MONGO_URL, function (err, db) {
            var dbo = db.db("chatapp");
            var logObj = {
                type: "error",
                message: "User " + req.user.email + " got an invalid login link",
                createdAt: new Date().toLocaleString('fr')
            };

            dbo.listCollections({name: "Logs"}).next(function (err, collinfo) {
                if (!collinfo) {
                    dbo.createCollection("Logs", function (err, res) {
                        if (err) throw err;
                        console.log("Collection Logs created!");
                    })
                }

                dbo.collection("Logs").insertOne(logObj, function (err, res) {
                    if (err) throw err;
                    // db.close();
                });

                dbo.collection("Logs").find({}).toArray(function (err, result) {
                    if (err) throw err;
                    db.close();
                });
            });
        });
        logger.info('Invalid URL !');
        return res.status(401).json('Invalid URL !');
    }

    req.session.authenticated = true;
    req.session.user = req.user.email;
    logger.info('Successfully logged !');

    MongoClient.connect(process.env.MONGO_URL, function (err, db) {
        if (err) throw err;

        var dbo = db.db("chatapp");
        var myobj = {email: req.user.email, startTime: new Date(), endTime: null, totalMinutes: null};

        dbo.listCollections({name: "DureeSession"}).next(function (err, collinfo) {
            if (!collinfo) {
                dbo.createCollection("DureeSession", function (err, res) {
                    if (err) throw err;
                    console.log("Collection created!");
                })
            }

            dbo.collection("DureeSession").insertOne(myobj, function (err, res) {
                if (err) throw err;
                console.log("Nouvelle session insérée.");
            });

            dbo.collection("DureeSession").find({}).toArray(function (err, result) {
                if (err) throw err;
                db.close();
            });
        });
    });

    MongoClient.connect(process.env.MONGO_URL, function (err, db) {
        var dbo = db.db("chatapp");
        var logObj = {
            type: "info",
            message: "User " + req.user.email + " successfully logged in",
            createdAt: new Date().toLocaleString('fr')
        };

        dbo.listCollections({name: "Logs"}).next(function (err, collinfo) {
            if (!collinfo) {
                dbo.createCollection("Logs", function (err, res) {
                    if (err) throw err;
                    console.log("Collection Logs created!");
                })
            }

            dbo.collection("Logs").insertOne(logObj, function (err, res) {
                if (err) throw err;
                console.log("Nouveau Log inséré.");
                // db.close();
            });

            dbo.collection("Logs").find({}).toArray(function (err, result) {
                if (err) throw err;
                db.close();
            });
        });
    });

    return res.status(200).json('Successfully logged !');
}

async function httpLogout(req, res) {
    logger.info('Successfully logged out !');
    const endDate = new Date();
    req.session.endDate = endDate;

    MongoClient.connect(process.env.MONGO_URL, async function (err, db) {
        if (err) throw err;

        var dbo = db.db("chatapp");
        var total = 0;

        async function getBeforeRes() {
            var test = await dbo.collection("DureeSession").find(
                {
                    email: req.body.email
                },
            ).toArray();

            return test[test.length - 1];
        }

        const beforeRes = await getBeforeRes();
        console.log("beforeRes : ", beforeRes);
        var calcul;
        if (beforeRes) {
            calcul = endDate - beforeRes.startTime;
        }
        else {
            calcul = 0;
        }
        var minutes = calcul / 60000;

        console.log("minutes : ", minutes);

        await dbo.collection("DureeSession").findOneAndUpdate(
            {
                email: req.body.email
            },
            {
                $set: {endTime: endDate, totalMinutes: minutes}
            },
            {
                returnDocument: "after",
                sort: {natural: -1}
            }
        );

        await dbo.collection('DureeSession').find(
            {
                email: {$ne: "duree_session_totale_6a934b454c61d9b8"},
                totalMinutes: {$ne: null},
            }
        ).toArray().then(async (res) => {
            res.map(session => {
                if (session.totalMinutes) {
                    total += session.totalMinutes;
                }
            });


            await dbo.collection("DureeSession").findOneAndUpdate(
                {
                    email: "duree_session_totale_6a934b454c61d9b8"
                },
                {
                    $set: {totalMinutes: total, startTime: null, endTime: null}
                },
                {
                    upsert: true,
                    returnDocument: "after",
                    sort: {natural: -1}
                }
            );
        });
        await db.close();
    });

    MongoClient.connect(process.env.MONGO_URL, function (err, db) {
        var dbo = db.db("chatapp");
        var logObj = {
            type: "info",
            message: "User " + req.body.email + " successfully logged out",
            createdAt: new Date().toLocaleString('fr')
        };

        dbo.listCollections({name: "Logs"}).next(function (err, collinfo) {
            if (!collinfo) {
                dbo.createCollection("Logs", function (err, res) {
                    if (err) throw err;
                    console.log("Collection Logs created!");
                })
            }

            dbo.collection("Logs").insertOne(logObj, function (err, res) {
                if (err) throw err;
                console.log("Nouveau Log inséré.");
                // db.close();
            });

            dbo.collection("Logs").find({}).toArray(function (err, result) {
                if (err) throw err;
                db.close();
            });
        });
    });

    req.session.destroy();
    return res.status(200).json('Successfully logged out !');
}

async function httpResetPassword(req, res) {
    const user = await User.findOne({where: {email: req.body.email}});
    if (!user) {
        return res.status(401).json('Email not found !');
    }
    try {

        const newToken = await createToken(user);
        sendEmail('receiver@esgi.fr', 'Reset password of your account !', '<a href="http://localhost:3000/reset-password-confirmation/' + newToken + '">Confirm Reset</a>');

        userDatabase.update({
            resetPasswordToken: newToken,
        }, {
            where: {
                email: req.body.email
            }
        });

        return res.status(200).json({
            message: 'Please check your email to reset your password !'
        });

    } catch (error) {

        if (error instanceof ValidationError) {
            logger.error(error.message);
            res.status(422).json({
                'message': error.message
            });
        } else {
            logger.error(error.message);
            res.status(500).json({
                'message': error.message
            });
        }
    }
}

async function httpResetPasswordConfirmation(req, res) {
    const user = await User.findOne({where: {resetPasswordToken: req.body.token}});
    if (!user) {
        return res.status(401).json('Invalid token !');
    }
    console.log('user touver');
    try {
        userDatabase.update({
            password: bcryptjs.hashSync(req.body.password, 10),
            resetPasswordToken: null,
        }, {
            where: {
                resetPasswordToken: req.body.token
            }
        });
        return res.status(200).json('Password successfully changed !');

    } catch (error) {

        if (error instanceof ValidationError) {
            logger.error(error.message);
            res.status(422).json({
                'message': error.message
            });
        } else {
            logger.error(error.message);
            res.status(500).json({
                'message': error.message
            });
        }
    }
}

module.exports = {
    httpSignup,
    httpSignin,
    httpVerifySigninUrl,
    httpLogout,
    httpResetPassword,
    httpResetPasswordConfirmation
};