const { getAllTotalUsers } = require('../../models/postgres/user.model');

async function httpGetAllAnalytics(req, res) {
    const users = await getAllTotalUsers();
    return res.status(200).json(users);
}

module.exports = {
    httpGetAllAnalytics
};