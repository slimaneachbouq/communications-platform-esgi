const express = require('express');
const { httpGetAllAnalytics } = require('./analytics.controller');

const technologiesRouter = express.Router();

technologiesRouter.get('/', httpGetAllAnalytics);

module.exports = technologiesRouter;