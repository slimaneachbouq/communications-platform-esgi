const { getAllTechnologies } = require('../../models/postgres/technology.model');

async function httpGetAllTechnologies(req, res) {
    const technologies = await getAllTechnologies();
    return res.status(200).json(technologies);
}

module.exports = {
    httpGetAllTechnologies
};