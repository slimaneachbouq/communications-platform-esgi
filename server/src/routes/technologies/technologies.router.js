const express = require('express');
const { httpGetAllTechnologies } = require('./technologies.controller');

const technologiesRouter = express.Router();

technologiesRouter.get('/', httpGetAllTechnologies);

module.exports = technologiesRouter;