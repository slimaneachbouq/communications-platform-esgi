const express = require('express');

const technologiesRouter = require('./technologies/technologies.router');
const securityRouter = require('./security/security.router');
const usersRouter = require('./users/users.router');
const messagesRouter = require('./messages/messages.router');
const analyticsRouter = require('./analytics/analytics.router');
const logsRouter = require('./logs/logs.router');
const reportsRouter = require('./reports/reports.router')

const api = express.Router();

api.use('/logs', logsRouter);
api.use('/user-info', usersRouter);
api.use('/technologies', technologiesRouter);
api.use('/analytics', analyticsRouter);
api.use('/reports', reportsRouter);
api.use('/new-report', reportsRouter);
api.use('/', securityRouter);
api.use('/', usersRouter);
api.use('/user-info', usersRouter);
api.use('/', messagesRouter);

api.use('/', (req, res) => {
    return res.status(200).json({"message": "Welcome to our api !"});
});

module.exports = api;