const { getUsersNotRelatedWithTheGivenUser, getFriends, searchFriends,getInvitations, saveFriendRequest, updateFriendRequest, removeFriendRequest } = require('../../models/postgres/friendRequest.model');
const { getConversations, getMessages, getAllTotalUsers, getUserInfo, updateUser } = require('../../models/postgres/user.model');
const {ValidationError} = require("sequelize");
const {formatError} = require("../../services/helpers");
const logger = require("../../services/logger");
const {verifyToken} = require("../../services/tokenManager");

async function httpGetUsers(req, res) {
    const users = await getUsersNotRelatedWithTheGivenUser(req.user.id);
    return res.status(200).json(users);
}

async function httpGetFriends(req, res) {
    const users = await getFriends(req.user.id);
    return res.status(200).json(users);
}

async function httpGetConversation(req, res) {
    const users = await getConversations(req.user.id);
    return res.status(200).json(users);
}

async function httpGetUserInfo(req, res) {
    const user = await getUserInfo(req.user.id);
    return res.status(200).json(user);
}

async function httpGetAllAnalytics(req, res) {
    const users = await getAllTotalUsers();
    return res.status(200).json(users);
}

async function httpGetInvitation(req,res){
    const invitations = await getInvitations(req.user.id);
    return res.status(200).json(invitations);
}

async function httpCreateInvitation(req, res) {
    try {
        const decoded = await verifyToken(req.body.jwt);
        const response = await saveFriendRequest([decoded.id, req.body.receiver]);
        logger.info(response);
        res.status(201).json({
            message: 'ok!'
        });
    } catch (err) {
        if (err instanceof ValidationError) {
            logger.error(formatError(err));
            res.status(422).json(formatError(err));
        } else {
            logger.error(err.message);
            res.status(500).json({
                message: err.message,
            });
        }
    }
}

async function httpUpdateInvitation(req, res) {
    try {
        const decoded = await verifyToken(req.body.jwt);
        const response = await updateFriendRequest([req.body.sender, decoded.id, req.body.choice]);
        logger.info(response);
        res.status(201).json({
            message: 'ok!'
        });
    } catch (err) {
        if (err instanceof ValidationError) {
            logger.error(formatError(err));
            res.status(422).json(formatError(err));
        } else {
            logger.error(err.message);
            res.status(500).json({
                message: err.message,
            });
        }
    }
}

async function httpRemoveInvitation(req, res) {
    try {
        const decoded = await verifyToken(req.body.jwt);
        const response = await removeFriendRequest([req.body.friend, decoded.id]);
        logger.info(response);
        res.status(201).json({
            message: 'ok!'
        });
    } catch (err) {
        if (err instanceof ValidationError) {
            logger.error(formatError(err));
            res.status(422).json(formatError(err));
        } else {
            logger.error(err.message);
            res.status(500).json({
                message: err.message,
            });
        }
    }
}


async function httpGetMessages(req, res) {
    const messages = await getMessages(req.user.id, req.params.friend);
    return res.status(200).json(messages);
}

async function httpSearchFriends(req, res) {
    const users = await searchFriends(req.user.id, req.params.value);
    return res.status(200).json(users);
}

async function httpUpdateProfileInfos(req, res) {
    try {
        const response = await updateUser(req.user.id, req.body)
        logger.info(response);
        res.status(201).json({
            message: 'ok!'
        });
    } catch (err) {
        if (err instanceof ValidationError) {
            logger.error(formatError(err));
            res.status(422).json(formatError(err));
        } else {
            logger.error(err.message);
            res.status(500).json({
                message: err.message,
            });
        }
    }
}

module.exports = {
    httpGetUsers,
    httpGetFriends,
    httpGetInvitation,
    httpCreateInvitation,
    httpUpdateInvitation,
    httpRemoveInvitation,
    httpGetConversation,
    httpGetMessages,
    httpSearchFriends,
    httpGetAllAnalytics,
    httpGetUserInfo,
    httpUpdateProfileInfos
}