const express = require('express');

const {
    httpGetUsers,
    httpGetFriends,
    httpGetConversation,
    httpGetMessages,
    httpSearchFriends,
    httpGetInvitation,
    httpCreateInvitation,
    httpUpdateInvitation,
    httpRemoveInvitation,
    httpGetUserInfo,
    httpUpdateProfileInfos
} = require('./users.controller');

const checkAuthentication = require('../../middlewares/checkAuthentication');

const userRouter = express.Router();

userRouter.get('/users', checkAuthentication, httpGetUsers);
userRouter.get('/user-friends', checkAuthentication, httpGetFriends);
userRouter.get('/user-invitations', checkAuthentication, httpGetInvitation);
userRouter.get('/user-conversations', checkAuthentication, httpGetConversation);
userRouter.post('/new-invitation', checkAuthentication, httpCreateInvitation);
userRouter.post('/update-invitation', checkAuthentication, httpUpdateInvitation);
userRouter.post('/remove-invitation', checkAuthentication, httpRemoveInvitation);
userRouter.get('/user-messages/:friend', checkAuthentication, httpGetMessages);
userRouter.get('/user-search-friends/:value', checkAuthentication, httpSearchFriends);
userRouter.get('/', checkAuthentication, httpGetUserInfo);
userRouter.get('/authenticated-user', checkAuthentication, (req, res) => {
    return res.json(req.user);
});
userRouter.post('/update-infos', checkAuthentication, httpUpdateProfileInfos)

module.exports = userRouter;