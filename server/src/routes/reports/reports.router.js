const express = require('express');
const { httpGetReports, httpCreateReport} = require('./reports.controller');

const reportsRouter = express.Router();

reportsRouter.get('/', httpGetReports);
reportsRouter.post('/new', httpCreateReport);

module.exports = reportsRouter;