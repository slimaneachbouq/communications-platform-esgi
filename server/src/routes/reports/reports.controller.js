const {getReports, saveReport} = require("../../models/postgres/report.model");
const logger = require("../../services/logger");
const {ValidationError} = require("sequelize");
const {formatError} = require("../../services/helpers");

async function httpGetReports(req, res) {
    const reports = await getReports();
    return res.status(200).json(reports);
}

async function httpCreateReport(req, res) {
    try {
        console.log(req.body.reason.trim());
        const response = await saveReport([req.body.createdBy, req.body.userReported, req.body.reason.trim()]);
        logger.info(response);
        res.status(201).json({
            message: 'ok!'
        });
    } catch (err) {
        if (err instanceof ValidationError) {
            logger.error(formatError(err));
            res.status(422).json(formatError(err));
        } else {
            logger.error(err.message);
            res.status(500).json({
                message: err.message,
            });
        }
    }
}

module.exports = {
    httpGetReports,
    httpCreateReport
};